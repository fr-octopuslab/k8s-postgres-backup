FROM debian:bullseye-slim

MAINTAINER Roland Laurès <roland@octopuslab.fr>

ARG BUILD_DATE
ARG VCS_REF
ARG VERSION
LABEL org.label-schema.build-date=$BUILD_DATE \
      org.label-schema.name="k8s-postgres-backup" \
      org.label-schema.description="Kubernetes image enabling backup of PostgreSQL databases and store them in Openstack Object storage" \
      org.label-schema.url="https://hub.docker.com/repository/docker/octopuslab/k8s-postgres-backup" \
      org.label-schema.vcs-ref=$VCS_REF \
      org.label-schema.vcs-url="https://gitlab.com/octopuslab-public/k8s-postgres-backup" \
      org.label-schema.vendor="Octopus Lab" \
      org.label-schema.version=$VERSION \
      org.label-schema.schema-version="1.0"

ENV PGHOST \
    PGUSER \
    PGPASSWORD \
    PGDATABASE  \
    PGPORT  \
    OS_AUTH_URL=https://auth.cloud.ovh.net \
    OS_AUTH_VERSION=3 \
    OS_USERNAME \
    OS_PASSWORD \
    OS_PROJECT_ID \
    OS_REGION_NAME \
    OS_CONTAINER

RUN apt-get update && \
    apt-get install -qq -y python3-swiftclient postgresql-client gzip trickle && \
    apt-get clean
