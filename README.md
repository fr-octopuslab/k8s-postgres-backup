# k8s-postgres-backup

Project to create a K8s CronJobin charge of backuping your database.

# Detailed description

## Version 1 goals

- [ ] Has a Dockerfile creating an image usable from a K8s cluster
  - [ ] Install dependencies:
    - pg_dump
    - gzip
    - swift (OpenStack client)
  - [ ] Environnement variables for configuration of the various applications
    - For pg_dump: PG* (PGUSER / PGPASSWORD / PGHOST / PGDATABASE / PGPORT)
    - For swift:
      - OS_AUTH_URL (default: 'https://auth.cloud.ovh.net')
      - OS_AUTH_VERSION (default: '3')
      - OS_USERNAME (no default)
      - OS_PASSWORD (no default)
      - OS_PROJECT_ID (no default)
      - OS_REGION_NAME (no default)
      - OS_CONTAINER (no default)
    - For trickle (bandwidth limiting):
      - TRICKLE_ENABLE (default unset):
        Enable or not bandwidth limitations (anything different from empty sttring will do)
      - TRICKLE_DOWNLOAD (default unset):
        kB/s limitation (if unset no download limitation)
      - TRICKLE_UPLOAD (default unset):
        kB/s limitation (if unset no upload limitation)
- [ ] Create a yaml file with the command to store
  - First make the backup weekly or find a way to configure it
- [ ] Create a command to backup and a command to restore
- [ ] Allow name change of the restored database

# Documentation

TODO: write how to use it.
